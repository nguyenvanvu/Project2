package com.example.workspace.project2;

import android.app.ActivityManager;
import android.app.DownloadManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get process running
        getProcess(this);
        // Lay ra chi tiet cac cuoc gọi
        getAllSms();
        getCallDetails();

        // Lịch sử download
        DownloadManager downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        Intent downloadmanager = new Intent(downloadManager.ACTION_VIEW_DOWNLOADS);
        downloadmanager.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(downloadmanager);
    }

    public void getAllSms() {
        StringBuffer sb = new StringBuffer();
        List<SMS> lstSms = new ArrayList<SMS>();
        SMS objSms = new SMS();
        Uri message = Uri.parse("content://sms/");
        ContentResolver cr = this.getContentResolver();

        Cursor c = cr.query(message, null, null, null, null);
        this.startManagingCursor(c);
        int totalSMS = c.getCount();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {

                objSms = new SMS();
                objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                objSms.setAddress(c.getString(c
                        .getColumnIndexOrThrow("address")));
                objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                objSms.setReadState(c.getString(c.getColumnIndex("read")));
                objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    objSms.setFolderName("inbox");
                } else {
                    objSms.setFolderName("sent");
                }

                lstSms.add(objSms);
                c.moveToNext();
            }
        }
        // else {
        // throw new RuntimeException("You have no SMS");
        // }
        c.close();
        sb.append("DetailAllSMS: ");
        for (SMS sms:lstSms) {
            sb.append("\nMessage:--- " + sms.getMsg() + " \nAddress:--- "
                    + sms.getAddress() + " \nFolderName:--- " + sms.getFolderName()
                    + " \nTime :--- " + sms.getTime()
                    + "\nReadState(Đọc trạng thái):---"+ sms.getReadState());
        }

        Log.d("SMS",sb+"");

    }

    public void getCallDetails() {
        StringBuffer sb = new StringBuffer();
        String strOrder = android.provider.CallLog.Calls.DATE + " DESC";
  /* Query the CallLog Content Provider */
        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null,
                null, null, strOrder);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Log :");
        while (managedCursor.moveToNext()) {
            String phNum = managedCursor.getString(number);
            String callTypeCode = managedCursor.getString(type);
            String strcallDate = managedCursor.getString(date);
            Date callDate = new Date(Long.valueOf(strcallDate));
            String callDuration = managedCursor.getString(duration);
            String callType = null;
            int callcode = Integer.parseInt(callTypeCode);
            switch (callcode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    callType = "Outgoing";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    callType = "Incoming";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    callType = "Missed";
                    break;
            }
            sb.append("\nPhone Number:--- " + phNum + " \nCall Type:--- "
                    + callType + " \nCall Date:--- " + callDate
                    + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();
        Log.d("getCallDetails: ",sb+"");
    }

    private void getProcess(Context context) {
        ActivityManager actvityManager = (ActivityManager)
                context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> pids = actvityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo runningProInfo : pids) {

            Log.d("Running Processes", "()()" + runningProInfo.processName);

        }
        int processid;
        long runTime;

        for (ActivityManager.RunningAppProcessInfo processInfo : pids) {
            String processName = processInfo.processName;
            processid = processInfo.pid;
            try {
                runTime = getRunTime(processid);
                Log.d("ProcessName: " + processName + ". RunTime = ", "" + runTime / 60000 + " phút");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static long getRunTime(final int pid) throws IOException {
        final String path = "/proc/" + pid + "/stat";
        final BufferedReader reader = new BufferedReader(new FileReader(path));
        final String stat;
        try {
            stat = reader.readLine();
        } finally {
            reader.close();
        }
        final String field2End = ") ";
        final String fieldSep = " ";
        final int fieldStartTime = 20;
        final int msInSec = 1000;
        try {
            final String[] fields = stat.substring(stat.lastIndexOf(field2End)).split(fieldSep);
            final long t = Long.parseLong(fields[fieldStartTime]);
            final int tckName = Class.forName("libcore.io.OsConstants").getField("_SC_CLK_TCK").getInt(null);
            final Object os = Class.forName("libcore.io.Libcore").getField("os").get(null);
            final long tck = (Long) os.getClass().getMethod("sysconf", Integer.TYPE).invoke(os, tckName);
            return t * msInSec / tck;
        } catch (final NumberFormatException e) {
            throw new IOException(e);
        } catch (final IndexOutOfBoundsException e) {
            throw new IOException(e);
        } catch (ReflectiveOperationException e) {
            throw new IOException(e);
        }
    }
}
